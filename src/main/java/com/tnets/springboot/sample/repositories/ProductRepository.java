package com.tnets.springboot.sample.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tnets.springboot.sample.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}